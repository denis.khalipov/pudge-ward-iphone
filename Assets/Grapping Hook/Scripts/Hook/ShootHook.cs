﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShootHook : MonoBehaviour
{
    private Transform whoShootTransform;                                            // who shoot
    public GameObject hookLeaderPerfab;                                             // hook leader perfab
    public GameObject hookLeaderObject;                                      // hook leader GameObject

    private float shootTime;                                                        // shoot time
    public float shootInterval = 0.2f;                                              // shoot interval
    HookLeader hookLeader;
    private Transform mTransform;                                                   // Reference to this transform



    public GameObject GetHook
    {
        get { return hookLeaderObject; }
    }
    void Awake()
    {

    }

    void Start()
    {
        mTransform = transform;
        whoShootTransform = mTransform.parent;

    }
    public void Hooook()
    {
        Shooting(mTransform.rotation);
    }

    private void Shooting(Quaternion hookRotation)
    {
        if (hookLeaderObject != null || Time.time - shootTime < shootInterval)
        {
            // если создан один хук, не создавать больше.

            return;
        }

        // save shoot time.
        shootTime = Time.time;
        // let owner to do shoot action
        // characterController.Shoot();
        // create hook gameObject
        hookLeaderObject = Instantiate(hookLeaderPerfab, mTransform.position, hookRotation) as GameObject;
        // get hook leader 
        hookLeader = hookLeaderObject.GetComponent<HookLeader>();
        // Instantiate the hook
        hookLeader.Init(mTransform);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
public class Hook : MonoBehaviour
{

	private Transform mTransform; 									// Reference to this transform.

	private List<Transform> hookObjects = new List<Transform> ();	// Enemies got hooked
	public float targetBondDamping = 10.0f; 						// The damping for bond target object
	public float targetBondDistance = 0.2f;							// The distance for bond target object
	public float hookRotateSpeed = 500;								// Hook rotating speed

	public HookEventListener hookEventListener = new HookEventListener(); // hook take back listener
    private bool HookShot = false;
	private Transform hookModelTransform;							// Hook model refence	
	private Transform ownerTrans;									// Hook owner
    private GameObject HookShotObject;
    [SerializeField] private GameObject[] SkinHook;
   
	public void setOwnerTrans(Transform owner){
		// set the owner of this hook
		ownerTrans = owner;
        if (ownerTrans.gameObject.GetComponent<PlayerController>() != null)
        {
            SkinHook[PlayerPrefs.GetInt("NumberHook", 0)].SetActive(true);
        }
        else
        {
            SkinHook[0].SetActive(true);
        }
	}
	
	void Awake(){
		mTransform = transform;
		hookModelTransform = mTransform.Find ("hook");
	}
	
	// Use this for initialization
	void Start ()
	{

	}

	void Update()
	{
		// rotate the hook
		//hookModelTransform.Rotate(Vector3.forward * Time.deltaTime * hookRotateSpeed, Space.Self);
	}

	
	void OnDestroy ()
	{
		// clear the enemies got hooked
		if (hookObjects.Count > 0) {
			if (ownerTrans != null) {
					foreach (Transform trans in hookObjects) {
							// Do not ignore collision between owner and enemy
							Physics.IgnoreCollision (ownerTrans.GetComponent<Collider>(), trans.GetComponent<Collider>(), false);
					}
			}
			hookObjects.Clear ();
           
		}
        if (HookShotObject != null)
        {
            if (HookShotObject.GetComponent<AiController>() != null )
            {

                HookShotObject.GetComponent<AiController>().YouInHookOrNo();
                HookShotObject.GetComponent<AiController>().ChecBleedignActive(false, 0);
                if (ownerTrans.GetComponent<PlayerController>() && HookShotObject.GetComponent<AiController>().Dead == true) //если плауер хукует и тот кто на хуке умер 
                {
                    ownerTrans.GetComponent<PlayerController>().SetKill(1);
                }
                if (ownerTrans.GetComponent<AiController>() && HookShotObject.GetComponent<AiController>().Dead == true) //если аишник хукует и тот кто на хуке умер 
                {
                    ownerTrans.GetComponent<AiController>().SetKill(1);
                }

            }
            else if (HookShotObject.GetComponent<PlayerController>() != null && HookShotObject.GetComponent<PlayerController>().Dead == false) //
            {
                HookShotObject.GetComponent<CapsuleCollider>().enabled = true;
                HookShotObject.GetComponent<Rigidbody>().useGravity = true;
                HookShotObject.GetComponent<PlayerController>().ChecBleedignActive(false, 0);
             
            }
            else if (ownerTrans.GetComponent<AiController>() && HookShotObject.GetComponent<PlayerController>().Dead == true) //если аишник хукует и тот кто на хуке умер 
            {
                ownerTrans.GetComponent<AiController>().SetKill(1);
                HookShotObject.GetComponent<PlayerController>().ChecBleedignActive(false, 0);
            }
        }
     
    }

	void FixedUpdate ()
	{
		// enemies follow the hook
		if (hookObjects.Count > 0 && mTransform != null) {
			foreach (Transform trans in hookObjects) {
				FollowHook (mTransform, trans);
			}
		}
	}

	void OnTriggerEnter(Collider collider) 
	{
        if (!HookShot)
        {
            if (collider.gameObject.tag == Tags.enemy)
            {
                // enemy get hooked by hook
                hookObjects.Add(collider.gameObject.transform);
                collider.GetComponent<AiController>().YouInHookOrNo();

                HookShotObject = collider.gameObject;

                if (ownerTrans.GetComponent<AiController>()!=null && collider.GetComponent<AiController>().GetCommand != ownerTrans.GetComponent<AiController>().GetCommand )
                {
                    ownerTrans.GetComponent<AiController>().Hithook(collider.GetComponent<AiController>().SetDamage(ownerTrans.GetComponent<AiController>().GetDamage));
                    ownerTrans.GetComponent<AiController>().LevelUp(100);
                    if (ownerTrans.GetComponent<AiController>().BleedingLevel > 0)
                    {
                        collider.GetComponent<AiController>().ChecBleedignActive(true, ownerTrans.GetComponent<AiController>().BleedingLevel);
                    }
                    //   collider.GetComponent<AiController>().SetHeals(ownerTrans.gameObject);
                }
               if(ownerTrans.GetComponent<PlayerController>() != null && collider.GetComponent<AiController>().GetCommand != true)
                {
                  ownerTrans.GetComponent<PlayerController>().Hithook(collider.GetComponent<AiController>().SetDamage(ownerTrans.GetComponent<PlayerController>().GetDamage)); //Попал
                    ownerTrans.GetComponent<PlayerController>().LevelUp(100);//попадание прибовляет лвл
                    if (ownerTrans.GetComponent<PlayerController>().BleedingLevel > 0)
                    {
                        collider.GetComponent<AiController>().ChecBleedignActive(true, ownerTrans.GetComponent<PlayerController>().BleedingLevel);
                    }

                }
                if (ownerTrans != null)
                    Physics.IgnoreCollision(ownerTrans.GetComponent<Collider>(), collider, true);

                // tell hook leader the hook should tack back.
                HookShot = true;
                hookEventListener.NotifyTakeBack();
            }
            if (collider.gameObject.tag == Tags.player)
            {
                // enemy get hooked by hook
                hookObjects.Add(collider.gameObject.transform);
                if (collider.GetComponent<PlayerController>() != null ) //для игрока
                {
                    collider.GetComponent<CapsuleCollider>().enabled = false;
                    collider.GetComponent<Rigidbody>().useGravity = false;
                    HookShotObject = collider.gameObject;
                    if (ownerTrans.GetComponent<AiController>().GetCommand != true)
                    {
                        ownerTrans.GetComponent<AiController>().Hithook(collider.GetComponent<PlayerController>().SetDamage(ownerTrans.GetComponent<AiController>().GetDamage));
                        ownerTrans.GetComponent<AiController>().LevelUp(100);

                        if (ownerTrans.GetComponent<AiController>().BleedingLevel > 0)
                        {
                            collider.GetComponent<PlayerController>().ChecBleedignActive(true, ownerTrans.GetComponent<AiController>().BleedingLevel);
                        }

                    }
                }


                if (ownerTrans != null)
                    Physics.IgnoreCollision(ownerTrans.GetComponent<Collider>(), collider, true);

                // tell hook leader the hook should tack back.
                HookShot = true;
                hookEventListener.NotifyTakeBack();
            }
            else if (collider.gameObject.tag == Tags.cylinder)
            {
                if (ownerTrans.GetComponent<PlayerController>() != null) //для игрока
                {
                    ownerTrans.GetComponent<CapsuleCollider>().enabled = false;
                    ownerTrans.GetComponent<Rigidbody>().useGravity = false;
                    HookShotObject = ownerTrans.gameObject;
                }
                if (ownerTrans.GetComponent<NavMeshAgent>() != null)//для бота
                {

                    ownerTrans.GetComponent<AiController>().YouInHookOrNo();
                    HookShotObject = ownerTrans.gameObject;
                }
                hookEventListener.NotifyHookSomething(mTransform.position);
                HookShot = true;
            }
           
        }
	}

	private void FollowHook (Transform prevNode, Transform follower)
	{
		// make follower follow the node
		Quaternion targetRotation = Quaternion.LookRotation (prevNode.position - follower.position, prevNode.up);
		targetRotation.x = 0f;
		targetRotation.z = 0f;
		follower.rotation = Quaternion.Slerp (follower.rotation, targetRotation, Time.deltaTime * targetBondDamping);

		Vector3 targetPosition = prevNode.position;
		targetPosition -= follower.rotation * Vector3.forward * targetBondDistance;
		targetPosition.y = follower.position.y;
		follower.position = Vector3.Lerp (follower.position, targetPosition, Time.deltaTime * targetBondDamping);

	}
	
}

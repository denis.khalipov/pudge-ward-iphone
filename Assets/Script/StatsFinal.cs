﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StatsFinal : MonoBehaviour
{
  
    [SerializeField] private Text NameText, Kill, Dead;
    void Start()
    {
        
    }

    public void SetStats(string name,int kill,int dead)
    {
        NameText.text = name;
        Kill.text = kill.ToString();
        Dead.text = dead.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.PlayerLoop;

public class GameController : Singleton<GameController>
{
    public enum StayGame
    {
        Game,
        Default
    }
    public StayGame Stay = StayGame.Default;
    [SerializeField] private AudioManager Audio;
    [SerializeField] private List<GameObject> RedCommand = new List<GameObject>();
    [SerializeField] private List<GameObject> BlueCommand = new List<GameObject>();
    [SerializeField] private Text RedCounText, BlueCountText, TimerText;
    [SerializeField] private GameObject[] RedTeamActive;
    [SerializeField] private GameObject[] BlueTeamActive;
    [SerializeField] private GameObject GridRed;
    [SerializeField] private GameObject GridBlue;
    [SerializeField] private GameObject StatsObject;
    [SerializeField] private GameObject FinalContainer;
    [SerializeField] private Text WinRedText, WinBlueText;
    [SerializeField] private Image StartTimer;
    [SerializeField] private Sprite[] StartSprite;
    [SerializeField] private GameObject ContainerOpenShop, ContainerCloseShop;
    [SerializeField] private Text AnimGoldText;
    [SerializeField] private Image LevelUpAnimText;
    [SerializeField] private Image[] RedTeamImageIcon;
    [SerializeField] private Image[] BlueTeamImageIcon;
    [SerializeField] private ScribleContainerButtonUpdate[] Data;
    [SerializeField] private GameObject ContainerSkill;
    [SerializeField] private GameObject GridSkill;
    private float StartPosSkillContainer;
    public ParticleSystem EffectHealthAi, EffectBlood;
    private int MaxCount;
    private float RedCount, BlueCount;
    private float GameTime, GameKill;
    private float TimerGame = 0;
    public bool FinishGameActive = false;
    public Sprite LifeIcon, DeadIcon;
    bool OpenContainer = false;


    public void SetRedCommand(GameObject Red)
    {
        RedCommand.Add(Red);
    }
    public void SetBlueCommand(GameObject Blue)
    {
        BlueCommand.Add(Blue);
        
    }

    public void DeletRedCommand(GameObject Red)
    {
        BlueCount++;
        RedCommand.Remove(Red);
        BlueCountText.text = BlueCount.ToString();
        StartCoroutine(ChecGameWin());
    }
    public void DeletBlueCommand(GameObject Blue)
    {
        RedCount++;
        BlueCommand.Remove(Blue);
        RedCounText.text = RedCount.ToString();
        StartCoroutine(ChecGameWin());
    }
    private IEnumerator ChecGameWin()
    {
        yield return new WaitForSeconds(0.1f);
        if (GameKill == 1)
        {
            if (!FinishGameActive)
            {
                if (BlueCount >= MaxCount)
                {
                    FinishGame();
                }
                else if (RedCount >= MaxCount)
                {
                    FinishGame();
                }
            }
        }
    }
    private void FinishGame()
    {
        AdmobController.Instance.ShowInterstitialsVideo();
        Stay = StayGame.Default;
        if (BlueCount >= MaxCount)
        {
            WinRedText.text = "LOSE";
            WinBlueText.text = "WIN";
        }
        else if (RedCount >= MaxCount)
        {
            WinRedText.text = "WiN";
            WinBlueText.text = "LOSE";
        }

        FinishGameActive = true;
        FinalContainer.SetActive(true);
        int SetTeamRed = PlayerPrefs.GetInt("RedTeamCount", 0);
        int SetTeamBlue = PlayerPrefs.GetInt("BlueTeamCount", 0);

        var PlayerInfo = Instantiate(StatsObject, GridRed.transform);
        PlayerInfo.GetComponent<StatsFinal>().SetStats(
            PlayerController.Instance.Name,
            PlayerController.Instance.KillCount,
            PlayerController.Instance.DeadCount
            );
        SetGoldController.Instance.SetInfo(PlayerController.Instance.KillCount);

        for (int i = 0; i < SetTeamRed; i++)
        {
            var R = Instantiate(StatsObject, GridRed.transform);
            R.GetComponent<StatsFinal>().SetStats(
                RedTeamActive[i].GetComponent<AiController>().Name,
                RedTeamActive[i].GetComponent<AiController>().KillCount,
                RedTeamActive[i].GetComponent<AiController>().DeadCount
                );

        }
        for (int i = 0; i < SetTeamBlue; i++)
        {
            var B = Instantiate(StatsObject, GridBlue.transform);
            B.GetComponent<StatsFinal>().SetStats(
                BlueTeamActive[i].GetComponent<AiController>().Name,
                BlueTeamActive[i].GetComponent<AiController>().KillCount,
                BlueTeamActive[i].GetComponent<AiController>().DeadCount
                );
        }

    }
    void Start()
    {
        StartPosSkillContainer = ContainerSkill.transform.localPosition.x;
        ActiveTeam();
        GameKill = PlayerPrefs.GetInt("GameKill", 0);
        GameTime = PlayerPrefs.GetInt("GameTime", 0);
        MaxCount = PlayerPrefs.GetInt("MaxCount", 0);
        TimerGame = PlayerPrefs.GetFloat("TimerGame", 0);
        BlueCountText.text = BlueCount.ToString();
        RedCounText.text = RedCount.ToString();
        StartGame();
        StartCoroutine(SetImageTeam());
       
    }
    
    public void ActiveAminTextGold(float count)
    {
        AnimGoldText.text = "+" + count.ToString();
        AnimGoldText.DOFade(1, 0.5f).OnComplete(() => { AnimGoldText.DOFade(0, 0.5f); });
        AnimGoldText.transform.DOScale(2, 1).OnComplete(() => { AnimGoldText.transform.DOScale(0, 0.5f); });
    }
    public void ActiveAminTextLevelUP()
    {
        LevelUpAnimText.transform.localScale = Vector3.zero;
        LevelUpAnimText.DOFade(0, 0);
        LevelUpAnimText.DOFade(1, 1).OnComplete(() => { LevelUpAnimText.DOFade(0, 1); });
        LevelUpAnimText.transform.DOScale(5, 2);
    }
    private IEnumerator SetImageTeam()
    {
        //добавляет каждому боту и игроку картинку с верху (Смерть , пудж икон , таймер)
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < RedCommand.Count; i++)
        {
            if (RedCommand[i].GetComponent<AiController>() != null)
            {
                RedCommand[i].GetComponent<AiController>().SetBarImage = RedTeamImageIcon[i];
            }
            //для игроков
            if (RedCommand[i].GetComponent<PlayerController>() != null)
            {
                RedCommand[i].GetComponent<PlayerController>().SetBarImage = RedTeamImageIcon[i];
            }
        }
        for (int i = 0; i < BlueCommand.Count; i++)
        {
            if (BlueCommand[i].GetComponent<AiController>() != null)
            {
                BlueCommand[i].GetComponent<AiController>().SetBarImage = BlueTeamImageIcon[i];

            }
        }
    }
    private void StartGame()
    {
        StartTimer.sprite = StartSprite[0];
        StartTimer.transform.DOScale(1, 1).OnComplete(delegate ()
        {
            StartTimer.transform.localScale = Vector3.zero;
            StartTimer.sprite = StartSprite[1];
            StartTimer.transform.DOScale(1, 1).OnComplete(delegate ()
            {
                StartTimer.transform.localScale = Vector3.zero;
                StartTimer.sprite = StartSprite[2];
                StartTimer.transform.DOScale(1, 1).OnComplete(delegate ()
                {
                    StartTimer.transform.localScale = Vector3.zero;
                    StartTimer.sprite = StartSprite[3];
                    StartTimer.transform.DOScale(2.5f, 0.3f).OnComplete(delegate ()
                    {
                        Stay = StayGame.Game;
                        Audio.PlaySound();
                        StartTimer.DOFade(0, 0.5f).OnComplete(delegate ()
                        {

                            StartTimer.gameObject.SetActive(false);
                        });
                    });
                });
            });
        });

    }
    private void ActiveTeam()
    {
        int SetTeamRed = PlayerPrefs.GetInt("RedTeamCount", 0);
        int SetTeamBlue = PlayerPrefs.GetInt("BlueTeamCount", 0);

        for (int i = 0; i < SetTeamRed; i++)
        {
            RedTeamActive[i].SetActive(true);
        }
        for (int i = 0; i < SetTeamBlue; i++)
        {
            BlueTeamActive[i].SetActive(true);
        }
    }
    public void OpenSkillContainer(bool _open)
    {
        GridSkill.SetActive(true);
        
        ContainerSkill.transform.DOLocalMoveX(_open ? StartPosSkillContainer - 83.5f : StartPosSkillContainer, 1).OnComplete(() =>
        {
            GridSkill.SetActive(_open ? true : false);

        });

    }
    
    void Update()
    {
        
       

        if (!FinishGameActive)
        {
            if (GameTime == 1)
            {
                TimerGame -= Time.deltaTime;
                if (TimerGame <= 0)
                {
                    FinishGame();
                    TimerGame = 0;
                }
            }
            else
            {
                TimerGame += Time.deltaTime;
            }
        }
        int minutes = Mathf.FloorToInt(TimerGame / 60F);
        int seconds = Mathf.FloorToInt(TimerGame - minutes * 60);
        TimerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    public void RestartRound()
    {
        SceneManager.LoadScene(1);
    }
    public void GoMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ActiveShop()
    {
        ContainerOpenShop.SetActive(true);
        ContainerOpenShop.transform.DOMoveX(ContainerCloseShop.transform.position.x, 1).OnComplete(() =>
        {
            OpenContainer = !OpenContainer;
            ContainerOpenShop.SetActive(OpenContainer);
        });
        ContainerCloseShop.transform.DOMoveX(ContainerOpenShop.transform.position.x, 1);
    }

    public Transform GetTargetBlue(Transform _player)
    {
        float distance = Mathf.Infinity;
        Transform closest = null;
        foreach (GameObject player in BlueCommand)
        {
            if (_player != player.transform)
            {
                float curDistance = Vector3.Distance(player.transform.position, _player.transform.position);
                if (curDistance < distance)
                {
                    closest = player.transform;
                    distance = curDistance;
                }
            }
        }
        return closest;
    }
    public Transform GetTargetRed(Transform _player)
    {
        float distance = Mathf.Infinity;
        Transform closest = null;
        foreach (GameObject player in RedCommand)
        {
            if (_player != player.transform)
            {
                float curDistance = Vector3.Distance(player.transform.position, _player.transform.position);
                if (curDistance < distance)
                {
                    closest = player.transform;
                    distance = curDistance;
                }
            }
        }
        return closest;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleScript : MonoBehaviour
{
    [SerializeField] private bool Time, Kill;
    [SerializeField] private float Timer;
    [SerializeField] private int KillCount;
    void Start()
    {
       
    }
    public void ChecGame()
    {
        if (Time)
        {
            PlayerPrefs.SetInt("GameTime", 1);
            PlayerPrefs.SetInt("GameKill", 0);
            PlayerPrefs.SetFloat("TimerGame", Timer);
         
        }
        else if(Kill)
        {
            PlayerPrefs.SetInt("GameTime", 0);
            PlayerPrefs.SetInt("GameKill", 1);
            PlayerPrefs.SetInt("MaxCount", KillCount);
            PlayerPrefs.SetFloat("TimerGame", 0);
        }
    }
    void Update()
    {
        
    }
}

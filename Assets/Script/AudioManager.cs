﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private AudioSource Music; 
    void Start()
    {
        Music = GetComponent<AudioSource>();
    }

    public void PlaySound()
    {
        int SoundActive = PlayerPrefs.GetInt("SoundActive", 1);
        float SoundVolue = PlayerPrefs.GetFloat("SoundValue", 0.3f);
        if (SoundActive == 1)
        {
            Music.Play();
            Music.volume = SoundVolue;
        }
    }
    void Update()
    {
        
    }
}

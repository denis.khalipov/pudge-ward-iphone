﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MenuController : Singleton<MenuController>
{
    [SerializeField] private Button SinglePlayerButton, BackButton,PlayGameBots;
    [SerializeField] private ToggleSettingController SettingControllerToggle;
    private int RedBotsCount;
    [SerializeField] private GameObject[] RedBots;
    private int BlueBotsCount;
    [SerializeField] private GameObject[] BlueBots;
    [SerializeField] private RectTransform StartMenu, GameBotMenu;
    [SerializeField] private InputField Name;
    [SerializeField] private Text NameTeam;
    private float Gold;
    [SerializeField] private Text[] GoldText;
    [SerializeField] private GameObject ContainerSetting;
    void Start()
    {
        Gold = PlayerPrefs.GetFloat("Gold", 0);
        UpdateGold();
        Name.text = PlayerPrefs.GetString("Name", "Name");
        SinglePlayerButton.onClick.AddListener(SinglePlayMod);
        BackButton.onClick.AddListener(SinglePlayMod);
        PlayGameBots.onClick.AddListener(PlayGame);
        PlayerPrefs.SetInt("BlueTeamCount", 0);
        PlayerPrefs.SetInt("RedTeamCount", 0);
    }
    void Update()
    {
        PlayerPrefs.SetString("Name", Name.text);
        NameTeam.text = PlayerPrefs.GetString("Name", "Name");
        if (Input.GetKeyDown(KeyCode.M))
        {
            Gold = 100000;
            UpdateGold();
            PlayerPrefs.SetFloat("Gold", Gold);
        }
    }
    public void VkGroup()
    {
        Application.OpenURL("https://vk.com/mobile_pudge_wars");
    }
    public void UpdateGold()
    {
        for (int i = 0; i < GoldText.Length; i++)
        {
            GoldText[i].text = Gold.ToString();
        }
    }
    
    public void AddAndDeletRedBots(int count)
    {
        if (RedBotsCount < RedBots.Length && count>0 || RedBotsCount !=0 && count<0)
            RedBotsCount += count;
        for (int i = 0; i < RedBotsCount; i++)
        {
            
            RedBots[i].SetActive(true);
           
        }
        for (int i = RedBotsCount; i < RedBots.Length; i++)
        {
            RedBots[i].SetActive(false);
        }

        PlayerPrefs.SetInt("RedTeamCount", RedBotsCount);
    }
    public void AddAndDeletBlueBots(int count)
    {
        if (BlueBotsCount < BlueBots.Length && count > 0 || BlueBotsCount != 0 && count < 0)
            BlueBotsCount += count;
        for (int i = 0; i < BlueBotsCount; i++)
        {

            BlueBots[i].SetActive(true);

        }
        for (int i = BlueBotsCount; i < BlueBots.Length; i++)
        {
            BlueBots[i].SetActive(false);
        }
        PlayerPrefs.SetInt("BlueTeamCount", BlueBotsCount);
    }
    private void PlayGame()
    {
        if(SettingControllerToggle.StartSettinGame())
        SceneManager.LoadScene(1);
        else
        {
            ContainerSetting.transform.DOScale(1.7f, 0.1f).OnComplete(() => { ContainerSetting.transform.DOScale(1.5f, 0.1f); });
        }
    }
    private void SinglePlayMod()
    {
        StartMenu.transform.DOMoveY(GameBotMenu.transform.position.y, 2);
        GameBotMenu.transform.DOMoveY(StartMenu.transform.position.y, 2);
    }
    
}

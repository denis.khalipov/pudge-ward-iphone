﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SettingController : MonoBehaviour
{
    [SerializeField] private Slider SoundSlider;
    [SerializeField] private AudioSource MenuSound;
    [SerializeField] private Toggle[] SoundToggle;
    [SerializeField] private Toggle[] JoystickToggle;
    private int OnOrOffSound;
    private int ActiveJoystick;
    void Start()
    {
       
        SoundSlider.value = PlayerPrefs.GetFloat("SoundValue", 0.5f);

        OnOrOffSound = PlayerPrefs.GetInt("SoundActive", 1);
        ActiveJoystick = PlayerPrefs.GetInt("JoystickActive", 1);
        ActiveSound(OnOrOffSound);
        if (OnOrOffSound ==1)
        {
            SoundToggle[0].isOn = true;
        }
        else
        {
            SoundToggle[1].isOn = true;
        }
        if (ActiveJoystick == 1)
        {
            JoystickToggle[0].isOn = true;
        }
        else
        {
            JoystickToggle[1].isOn = true;
        }
    }
   
    // Update is called once per frame
    void Update()
    {
        if (OnOrOffSound == 1)
        {
            MenuSound.volume = SoundSlider.value;
        }

        PlayerPrefs.SetFloat("SoundValue", SoundSlider.value);
    }
    public void ActiveSound(int i)
    {
        OnOrOffSound = i;
        bool active = i == 1;
        MenuSound.enabled = active;
        PlayerPrefs.SetInt("SoundActive", OnOrOffSound);
    }
    public void ActiveJoystickSave(int i)
    {
       
        ActiveJoystick = i;
        PlayerPrefs.SetInt("JoystickActive", ActiveJoystick);
    }
  
}

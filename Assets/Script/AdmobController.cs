﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
public class AdmobController : Singleton<AdmobController>
{
    private RewardedAd RewardX2gold;
    private InterstitialAd AdInterstitials;
    private BannerView bannerView;
    public RewardedAd GetReward => RewardX2gold;
    void Start()
    {
        DontDestroyOnLoad(this);
        RequestRewardX2Gold();
        RequestInterstitial();
        RequestBanner();
        StartCoroutine(LoadReward());
    }
    private IEnumerator LoadReward()
    {
        while (true)
        {
            yield return new WaitForSeconds(60);

            if (!RewardX2gold.IsLoaded())
                RequestRewardX2Gold();
            if (!AdInterstitials.IsLoaded())
                RequestInterstitial();
            RequestBanner();
        }
    }
    private void RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-2298480941545474/8422228032";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-2298480941545474/2406773589";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-2298480941545474/8230656342";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-2298480941545474/5993475437";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        this.AdInterstitials = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        this.AdInterstitials.LoadAd(request);
    }
    public void ShowInterstitialsVideo()
    {
        if (AdInterstitials != null)
        {
            if (AdInterstitials.IsLoaded())
            {
                AdInterstitials.Show();
            }

        }
    }
    private void RequestRewardX2Gold()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-2298480941545474/5412921313";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-2298480941545474/1591002472";
#else
        string adUnitId = "unexpected_platform";
#endif
        this.RewardX2gold = new RewardedAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        this.RewardX2gold.OnUserEarnedReward += HandleUserEarnedRewardGold;
        this.RewardX2gold.LoadAd(request);
    }
    private void HandleUserEarnedRewardGold(object sender, Reward args)
    {
        SetGoldController.Instance.SetX2Gold();
    }
    public void ShowX2GoldClick()
    {
        if (RewardX2gold.IsLoaded())
        {
            RewardX2gold.Show();
        }
    }
}

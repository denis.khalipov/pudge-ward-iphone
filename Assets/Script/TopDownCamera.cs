﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownCamera : MonoBehaviour
{
    public Transform m_Target;
    public float m_Height = 10f;
    public float m_Distane = 20f;

    public float m_Angle = 45f;

    public float m_ShoothSpeed = 0.5f;

    private Vector3 refVelocity;

    void Start()
    {
        HandLeCamera();
    }

    // Update is called once per frame
    void Update()
    {
        HandLeCamera();
    }
    protected virtual void HandLeCamera()
    {
        if (!m_Target)
        {
            return;
        }

        Vector3 wordlPosition = (Vector3.forward * -m_Distane) + (Vector3.up * m_Height);

        Vector3 RotatedVector = Quaternion.AngleAxis(m_Angle, Vector3.up) * wordlPosition;

        Vector3 flatTargetPostion = m_Target.position;
        flatTargetPostion.y = 0;
        Vector3 finalPosition = flatTargetPostion + RotatedVector;

        transform.position = Vector3.SmoothDamp(transform.position, finalPosition, ref refVelocity, m_ShoothSpeed);
        transform.LookAt(flatTargetPostion);
        Mathf.Clamp(flatTargetPostion.z,-3,6);
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataContainers : Singleton<DataContainers>
{
    [SerializeField] private ScribleContainerButtonUpdate[] DataShopHook;
    public ScribleContainerButtonUpdate[] GetDataShopHook
    {
        get { return DataShopHook; }
    }
    [SerializeField] private ScribleContainerButtonUpdate1[] DataShopChein;
    public ScribleContainerButtonUpdate1[] GetDataShopChein
    {
        get { return DataShopChein; }
    }


}

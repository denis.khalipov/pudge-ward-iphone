﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class ListModelButton 
{
    public List<ModelButtonHook> ButtonModelsHook = new List<ModelButtonHook>();
    public List<ModelButtonChein> ButtonModelsChein = new List<ModelButtonChein>();
}
public enum TypesButton
{
    A1,
    A2,
    A3,
    A4,
    A5,
    A6,
    A7,
    A8,
    A9,
    A10,
    A11,
    A12,
    A13,
    A14,
    A15,
    A16,
    A17,
    A18,
    A19,
    A20,
    A21,
}
[Serializable]
public class ModelButtonHook
{
    public int Active;
}
[Serializable]
public class ModelButtonChein
{
    public int Active;
}
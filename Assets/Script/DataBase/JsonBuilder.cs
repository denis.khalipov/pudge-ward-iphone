﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class JsonBuilder : Singleton<JsonBuilder>
{

    private ScribleContainerButtonUpdate[] DataButtonHook;
    private ScribleContainerButtonUpdate1[] DataButtonChein;
    [SerializeField] private ListModelButton LMF;
    public string PathButtonJsonHook;
    public string PathButtonJsonChein;
    private void Awake()
    {
     
    }
    private void Start()
    {
        LoadData();
    }
    public ListModelButton GetButtonData
    {
        get { return LoadButtonData(); }
    }

    public ListModelButton SetSaveButton
    {
        set { SaveButton(value); }
    }
    private void LoadData()
    {

#if UNITY_ANDROID && !UNITY_EDITOR
        PathButtonJsonHook = Path.Combine(Application.persistentDataPath, "SaveButtonHook.json");
         PathButtonJsonChein = Path.Combine(Application.persistentDataPath, "SaveButtonChein.json");
#else
        PathButtonJsonHook = Application.persistentDataPath + "/SaveButtonHook.json";
        PathButtonJsonChein = Application.persistentDataPath + "/SaveButtonChein.json";
#endif

        if (!File.Exists(PathButtonJsonHook))
        {
            File.Create(PathButtonJsonHook).Dispose();//если нету файла создать файл
            LMF = new ListModelButton() { ButtonModelsHook = new List<ModelButtonHook>(), ButtonModelsChein = new List<ModelButtonChein>() };
            DataButtonHook = DataContainers.Instance.GetDataShopHook;
            for (int i = 0; i < DataButtonHook.Length; i++)
            {
                if(i == 0)
                {
                    ModelButtonHook MF = new ModelButtonHook()
                    {
                        Active = 1
                    };
                    LMF.ButtonModelsHook.Add(MF);
                }
                else
                {
                    ModelButtonHook MF = new ModelButtonHook()
                    {
                        Active = 0
                    };
                    LMF.ButtonModelsHook.Add(MF);
                }
              
            }
            DataButtonChein = DataContainers.Instance.GetDataShopChein;
            for (int i = 0; i < DataButtonChein.Length; i++)
            {
                if (i == 0)
                {
                    ModelButtonChein MF1 = new ModelButtonChein()
                    {
                        Active = 1
                    };
                    LMF.ButtonModelsChein.Add(MF1);
                }
                else
                {
                    ModelButtonChein MF1 = new ModelButtonChein()
                    {
                        Active = 0
                    };
                    LMF.ButtonModelsChein.Add(MF1);
                }

            }
            SaveButton(LMF);
        }
        LMF = LoadButtonData();
        ControllerSpawnLevels.Instance.SetData = LMF;
    }
    private ListModelButton LoadButtonData()
    {
        var jsonData = File.ReadAllText(PathButtonJsonHook);
        return JsonUtility.FromJson<ListModelButton>(jsonData);
    }
    private void SaveButton(ListModelButton _data)
    {
        string dataAsJson = JsonUtility.ToJson(_data, true);
        File.WriteAllText(PathButtonJsonHook, dataAsJson);
    }

}

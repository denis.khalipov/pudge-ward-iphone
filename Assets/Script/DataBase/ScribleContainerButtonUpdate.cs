﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu(fileName = "New Button", menuName = "Button Data", order = 51)]
public class ScribleContainerButtonUpdate : ScriptableObject
{
    [SerializeField] private string Name;
    [SerializeField] private int Cost;
    [SerializeField] private Sprite IconSprite;
    [SerializeField] private GameObject Model;
    [SerializeField] private int Number;


    public int GetCost
    {
        get { return Cost; }
    }
    public string GetName
    {
        get { return Name; }
    }
    public Sprite GetIcon
    {
        get { return IconSprite; }
    }
    public GameObject GetModel
    {
        get { return Model; }
    }
    public int GetNumber
    {
        get { return Number; }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class ButtonHookScript : MonoBehaviour
{

    [SerializeField] private int Number;
    [SerializeField] private int Active;
    [SerializeField] private int Cost;
    [SerializeField] private Image Icon;
    [SerializeField] private Text CostText;
    [SerializeField] private Text Name;
    [SerializeField] private Button ButtonClick;
    [SerializeField] private GameObject ActiveButtonGameObject;
    [SerializeField] private Sprite[] SkinButton;
    [SerializeField] private Image SkinButtonActive;
    public int GetActive
    {
        get { return Active; }
    }
    private void Start()
    {
        ButtonClick.onClick.AddListener(Buy);
    }
    public void InfoButton(Sprite _icon,int _cost,int _active, string _name, int number)
    {
        Icon.sprite = _icon;
        Cost = _cost;
        Active = _active;
        Name.text = _name;
        Number = number;
        CostText.text = Cost.ToString();
        ActiveButton();
    }
    public void ActiveButton()
    {

       if(Active== 1)
        {
            ButtonClick.interactable = true;
            if (PlayerPrefs.GetInt("NumberHook", 0) == Number)
            {
                SkinButtonActive.sprite = SkinButton[1];
                ActiveButtonGameObject.SetActive(true);
                CostText.gameObject.SetActive(false);
            }
            else
            {
                SkinButtonActive.sprite = SkinButton[0];
                ActiveButtonGameObject.SetActive(false);
                CostText.gameObject.SetActive(true);
                CostText.text = "Free";
            }
        }
        else if(Cost <= PlayerPrefs.GetFloat("Gold", 0))
        {
            ButtonClick.interactable = true;
        }
        else
        {
            ButtonClick.interactable = false;
        }
    }
    private void Buy()
    {
        if (Cost <= PlayerPrefs.GetFloat("Gold",0) && Active != 1)
        {
            Active = 1;
            PlayerPrefs.SetInt("NumberHook", Number);
            ControllerSpawnLevels.Instance.ChecActiveButton();

        }
        else if(Active == 1)
        {
            PlayerPrefs.SetInt("NumberHook", Number);
            ControllerSpawnLevels.Instance.ChecActiveButton();
        }
    }
}

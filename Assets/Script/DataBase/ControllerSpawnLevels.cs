﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerSpawnLevels : Singleton<ControllerSpawnLevels>
{
    [SerializeField] private RectTransform ContainerSpawnHook,ContainerSpawnChein;
    [SerializeField] private GameObject ButtonSpawnHook, ButtonSpawnChein;
    private ListModelButton Data;
    private ScribleContainerButtonUpdate[] ScribleHook;
    private ScribleContainerButtonUpdate1[] ScribleChein;
    [SerializeField] private JsonBuilder Json;
    private bool AllButtonSpawn = false;
    public List<ButtonHookScript> AllButtonHook = new List<ButtonHookScript>();
    public List<ButtonCheinScript> AllButtonChein = new List<ButtonCheinScript>();
    public ListModelButton SetData
    {
        set { Data = value;
            SpawnAllButtonHook();
            SpawnAllButtonChein();
            AllButtonSpawn = true;
        }
    }
    
    void Start()
    {
        ScribleHook = DataContainers.Instance.GetDataShopHook;
        ScribleChein = DataContainers.Instance.GetDataShopChein;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Save();
        }
    }
    private void SpawnAllButtonHook()
    {
        ScribleHook = DataContainers.Instance.GetDataShopHook;
        for (int i = 0; i < Data.ButtonModelsHook.Count; i++)
        {
         var Button = Instantiate(ButtonSpawnHook,ContainerSpawnHook);
            Button.GetComponent<ButtonHookScript>().InfoButton(
                ScribleHook[i].GetIcon,
                ScribleHook[i].GetCost,
                Data.ButtonModelsHook[i].Active,
                ScribleHook[i].GetName,
                 ScribleHook[i].GetNumber
                );
            AllButtonHook.Add(Button.GetComponent<ButtonHookScript>());
           
        }
       
    }
    private void SpawnAllButtonChein()
    {
        ScribleChein = DataContainers.Instance.GetDataShopChein;
        for (int i = 0; i < Data.ButtonModelsChein.Count; i++)
        {
            var Button = Instantiate(ButtonSpawnChein, ContainerSpawnChein);
            Button.GetComponent<ButtonCheinScript>().InfoButton(
                ScribleChein[i].GetIcon,
                ScribleChein[i].GetCost,
                Data.ButtonModelsChein[i].Active,
                ScribleChein[i].GetName,
                 ScribleChein[i].GetNumber
                );
            AllButtonChein.Add(Button.GetComponent<ButtonCheinScript>());

        }
        AllButtonSpawn = true;
    }
    public void ChecActiveButton()
    {
        Save();
        for (int i = 0; i < AllButtonHook.Count; i++)
        {
            AllButtonHook[i].ActiveButton();
        }
        for (int i = 0; i < AllButtonChein.Count; i++)
        {
            AllButtonChein[i].ActiveButton();
        }

    }
    public void Save()
    {
        if (AllButtonSpawn)
        {
            for (int i = 0; i < Data.ButtonModelsHook.Count; i++)
            {
                Data.ButtonModelsHook[i].Active = AllButtonHook[i].GetComponent<ButtonHookScript>().GetActive;
            }
            Json.SetSaveButton = Data;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using TMPro;

public class AiController : MonoBehaviour
{

    [SerializeField] private Animator Anim;
    [SerializeField] private ShootHook ShootHooks;
    public bool AnimHook = false;
    [SerializeField] private LayerMask LayerMask;
    Vector3 NewPos;
    bool GoMove = true;
    Transform target;
    bool HookAttack = false;
   [SerializeField] private bool RedCommand = false;
    [SerializeField] private bool BlueCommand = false;
    private float ReloadingHook = 10;
   
    public bool Dead = false;
    Vector3 PosSpawn;
    [SerializeField] private Canvas CanvasRotate;
    [SerializeField] private Image Bar;
    private CapsuleCollider ColliderThis;
    private NavMeshAgent Agent;
    private Rigidbody rb;
    private bool YouInHook = false;
    public int KillCount;
    public int DeadCount;
    public string Name;
    RaycastHit hit;

    public int DistansHook = 0;
    public float SpeedHook = 0;
    public float ScaleHook = 0;
    private int levelCount = 1;
    private float MaxCostLevel = 100;
    private float MinCostLevel = 0;
    [SerializeField] private ParticleSystem LevelUpEffect;
    [SerializeField] private TextMeshProUGUI TextCanvasLevel;
    private Image BarIcon;
    private Sprite IconLife, IconDead;
    private Text TextDead;
    private float MaxHeals = 300;
    private float MinHeal = 0;
    private float Damage = 100;
    private int Gold = 0;
    private float SpeedMoveLevel = 0;
    private int HealseSetLevel = 0;
    private int WampirizmLevel = 0;
    private int DamageLevel = 0;
    private int RegenerationLevel = 0;
    public int BleedingLevel = 0;
    private float CountSetBleeding = 0;
    private ParticleSystem HealthEffect;
    private ParticleSystem Blood;
    public float GetDamage
    {
        get { return Damage + (DamageLevel * 20); }
    }
    void Start()
    {
     //   BleedingLevel = 1;
        MinHeal = MaxHeals;
        rb = GetComponent<Rigidbody>();
        ColliderThis = GetComponent<CapsuleCollider>();
        Agent = GetComponent<NavMeshAgent>();
        PosSpawn = transform.position;
        ReloadingHook = Random.Range(0, 10);
        if (RedCommand)
        {
            GameController.Instance.SetRedCommand(this.gameObject);
            Bar.color = Color.red;
        }
        else
        {
            GameController.Instance.SetBlueCommand(this.gameObject);
            Bar.color = Color.blue;
        }
        Bar.fillAmount = MinHeal / MaxHeals;
        TextCanvasLevel.text = levelCount.ToString();
    }

    public Image SetBarImage
    {
        set { BarIcon = value;
            IconLife = GameController.Instance.LifeIcon;
            IconDead = GameController.Instance.DeadIcon;
            BarIcon.sprite = IconLife;
        }
    }
    public void SetKill(int dead)
    {
        if (dead == 1)
        {
            KillCount++;
            Gold += 50;
          //  GameController.Instance.ActiveAminTextGold(50);
            // LevelUp(100); за убийство опыт
        }
        ChecGold();
    }
    public void Hithook(int i) //надо переписать к хуям эту хуету 
    {

        Gold += 10; //за попадание 10 голд
      //  GameController.Instance.ActiveAminTextGold(10);
        ChecGold();
    }
    private void ChecGold()
    {
        if (Gold >= 100)
        {
            int random = Random.Range(0, 6);
            switch (random)
            {
                case 0: // if a is an integer
                    Agent.speed += 0.5f;
                    SpeedMoveLevel++;
                    break;
                case 1: // if a is a string
                    HealseSetLevel++;
                    MaxHeals += 50;
                    MinHeal += 50;
                    Bar.fillAmount = MinHeal / MaxHeals;
                    break;
                case 2:
                    WampirizmLevel++;
                    break;
                case 3:
                    DamageLevel++;
                    break;
                case 4:
                    RegenerationLevel++;
                   
                    if (RegenerationLevel == 1)
                    {
                        StartCoroutine(RegenHp());
                    }
                    break;
                case 5:
                    BleedingLevel++;
                    break;
            }
            Gold -= 100;
        }

    }
    public void ChecBleedignActive(bool active, float count)
    {
        CountSetBleeding = 0;
        CountSetBleeding = count*0.7f;
        if (active)
        {

            if (Blood == null)
            {
                Blood = Instantiate(GameController.Instance.EffectBlood, transform);
            }
            if (Blood != null)
                Blood.Play();
            StartCoroutine(BleedingActive());
        }
        else
        {
            if (Blood != null)
                Blood.Stop();
            StopCoroutine(BleedingActive());
        }
    }
    private IEnumerator BleedingActive()
    {
        while (MinHeal > 0)
        {
            yield return new WaitForSeconds(0.3f);
            if (MinHeal > 0 && !Dead)
            {
                MinHeal -= CountSetBleeding;
                Bar.fillAmount = MinHeal / MaxHeals;
                SetDamage(0);
            }
            else
            {
                Dead = true;
                StopCoroutine(BleedingActive());
            }
        }


    }
    private void ChecWampirizm()
    {
        
        if (WampirizmLevel > 0 && !Dead)
        {
           if(HealthEffect == null)
            {
                HealthEffect = Instantiate(GameController.Instance.EffectHealthAi, transform);
               
            }

            HealthEffect.Play();
             MinHeal += 10 * WampirizmLevel;
            if (MinHeal > MaxHeals)
            {
                MinHeal = MaxHeals;
            }
            Bar.fillAmount = MinHeal / MaxHeals;
        }
    }
    public void LevelUp(int CostHook)
    {
        ChecWampirizm();
        MinCostLevel += CostHook;
        
        if (MaxCostLevel <= MinCostLevel)
        {
            LevelUpEffect.Play();
            levelCount++;
            TextCanvasLevel.text = levelCount.ToString();
            if (MinCostLevel > MaxCostLevel)
            {
                MinCostLevel = MinCostLevel - MaxCostLevel;
            }
            else
            {
                MinCostLevel = 0;
            }
            MaxCostLevel = 100 * levelCount;
            SetHookStats();
        }

    }
    private void SetHookStats()
    {
        int random = Random.Range(0, 3);
        if(random == 0)
        DistansHook += 10;
        if (random == 1)
            SpeedHook += 0.02f; ;
        if (random == 2)
            ScaleHook += 0.15f;
    }
    public bool GetCommand
    {
        get {return RedCommand; }
    }
    public int SetDamage(float _damage)
    {
        MinHeal -= _damage;

        Bar.fillAmount = MinHeal / MaxHeals;
        if (MinHeal <= 0 && !GameController.Instance.FinishGameActive )
        {
            
            StopCoroutine(BleedingActive());
            BarIcon.sprite = IconDead;
            DeadCount++;
            Dead = true;
            Anim.SetTrigger("Dead");
            Anim.SetInteger("Stay", 10);

            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<CapsuleCollider>().enabled = false;
            Agent.enabled = false;
            if (RedCommand)
            {
                GameController.Instance.DeletRedCommand(this.gameObject);
            }
            else
            {
                GameController.Instance.DeletBlueCommand(this.gameObject);
            }
            StartCoroutine(Respawn());
            CanvasRotate.enabled = false;
            return 1;
        }
        return 0;
    }
    
    public void YouInHookOrNo()
    {
            YouInHook = !YouInHook;
       
        if (!Dead)
        {
            rb.isKinematic = YouInHook;
            Agent.enabled = !YouInHook;
            ColliderThis.enabled = !YouInHook;
            SpawnRandomPos();
        }
        
    }
    private IEnumerator RegenHp()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (MinHeal < MaxHeals && !Dead)
            {
                MinHeal += RegenerationLevel * 2;
                Bar.fillAmount = MinHeal / MaxHeals;
            }
        }
    }
    void Update()
    {

        if (GameController.Instance.Stay == GameController.StayGame.Game)
        {
            CanvasRotate.transform.LookAt(CanvasRotate.transform.position + Camera.main.transform.rotation * Vector3.back,
                  Camera.main.transform.rotation * Vector3.up);


            if (!Dead && !GameController.Instance.FinishGameActive)
            {
                if (GoMove && !HookAttack)
                {
                    SpawnRandomPos();
                }
                else
                {
                    Anim.SetInteger("Stay", 1);
                    if (Vector3.Distance(transform.position, NewPos) < 2)
                    {
                        GoMove = true;
                    }
                }

                if (ReloadingHook <= 0)
                {
                    StartCoroutine(HookFire());
                    ReloadingHook = Random.Range(5, 7);
                }
                else
                {
                    ReloadingHook -= Time.deltaTime;
                }

            }
        }
    }
    private IEnumerator Respawn()
    {
        if (TextDead == null)
            TextDead = BarIcon.gameObject.GetComponentInChildren<Text>();
        TextDead.gameObject.SetActive(true);
        for (float i = 6; i > 0; i--)
        {
            TextDead.text = i.ToString();
            yield return new WaitForSeconds(1);
        }
        Agent.enabled = false;
        Dead = false;
        transform.position = PosSpawn;
       
        MinHeal = MaxHeals;
       
        ReloadingHook = Random.Range(5, 10);

        if (RedCommand)
        {
            GameController.Instance.SetRedCommand(this.gameObject);
        }
        else
        {
            GameController.Instance.SetBlueCommand(this.gameObject);
        }
        GoMove = true;
        HookAttack = false;
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<CapsuleCollider>().enabled = true;

        CanvasRotate.enabled = true;
        Bar.fillAmount = MinHeal / MaxHeals;
        BarIcon.sprite = IconLife;
        TextDead.gameObject.SetActive(false);
    }
    private IEnumerator HookFire()
    {
       
        if (RedCommand)
        {
            target = GameController.Instance.GetTargetBlue(transform);
        }
        if(BlueCommand)
        {
            target = GameController.Instance.GetTargetRed(transform);
        }
        if (target != null)
        {
            Agent.enabled = false;
            HookAttack = true;
            Anim.SetTrigger("Hook");
            var targetPoint = target.position;
            Vector3 TargetPos = new Vector3(targetPoint.x, transform.position.y, targetPoint.z);
            Quaternion neededRotation = Quaternion.LookRotation(TargetPos - transform.position);
            
            transform.rotation = Quaternion.RotateTowards(transform.rotation, neededRotation, 10000);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, Random.Range(transform.eulerAngles.y - 5, transform.eulerAngles.y + 5), transform.eulerAngles.z);
            if (!YouInHook)
            {
                Agent.enabled = true;
                GoMove = true;
            }
            yield return new WaitForSeconds(0.2f);
            ShootHooks.Hooook();
        }
     
        yield return new WaitForSeconds(0.2f);
        HookAttack = false;
        //SpawnRandomPos();
    }
        
    
    public void SpawnRandomPos()
    {
        if (!Dead && !YouInHook)
        {
           
            if (Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.up*2), out hit, Mathf.Infinity, LayerMask))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up*2) * hit.distance, Color.yellow);

            }
            if (hit.collider == null)
            {
                GoMove = true;
                return;
            }
            GoMove = false;
            if (hit.collider != null)
            {
                if (hit.transform.gameObject.tag == "RightGround")
                {
                    NewPos = new Vector3(Random.Range(3.5f, 12), 0.3f, Random.Range(9.3f, -9.3f));
                }
                else
                {
                    NewPos = new Vector3(Random.Range(-10, -1f), 0.3f, Random.Range(9.3f, -9.3f));
                }
                Agent.enabled = true;
                Agent.SetDestination(NewPos);
            }
        }
    }
  
}

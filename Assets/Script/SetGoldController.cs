﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SetGoldController : Singleton<SetGoldController>
{
    [SerializeField] private Text KillText, GoldText;
    [SerializeField] private Button SetGoldButton, SetX2GoldButton;
    [SerializeField] private AdmobController Admob;
    private float Gold;
    void Start()
    {
        SetGoldButton.onClick.AddListener(SetCurrenGold);
      
    }

    public void SetInfo(float _kill)
    {
        Gold = _kill * 3;
        KillText.text = _kill.ToString();
        GoldText.text = Gold.ToString();
        if (Admob.GetReward.IsLoaded())
        {
            SetX2GoldButton.gameObject.SetActive(true);
            SetX2GoldButton.onClick.AddListener(ShopRewardX2);
        }
    }
    private void SetCurrenGold()
    {
        float CurrenGold = PlayerPrefs.GetFloat("Gold", 0);
        CurrenGold += Gold;
        PlayerPrefs.SetFloat("Gold", CurrenGold);
        gameObject.SetActive(false);
       
    }
    private void ShopRewardX2()
    {
        Admob.ShowX2GoldClick();
    }
    public void SetX2Gold()
    {
        float CurrenGold = PlayerPrefs.GetFloat("Gold", 0);
        CurrenGold += Gold*2;
        PlayerPrefs.SetFloat("Gold", CurrenGold);
        gameObject.SetActive(false);
    }
}

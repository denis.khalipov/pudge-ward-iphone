﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerController : Singleton<PlayerController>
{
    [SerializeField] private GameObject Sun;
    [SerializeField] private float Speed;
    [SerializeField] private float RotateSpeed;
    [SerializeField] private Animator Anim;
    [SerializeField] private ShootHook ShootHooks;
    public bool AnimHook = false;
    [SerializeField] private FloatingJoystick Joystick_1;
    [SerializeField] private FixedJoystick Joystick_2;
    [SerializeField] private GameObject ArroyHook, JoystickIcon;
    [SerializeField] private Canvas CanvasRotate;
    [SerializeField] private Image Bar;
    [SerializeField] private Button ButtonHook;
    [SerializeField] private Text KDHOOkTExt,KdhookText_2;
    [SerializeField] private Image BakkGroundHookTimer;
    [SerializeField] private Image BarLevels;
    [SerializeField] private Text LevelsText;
    [SerializeField] private Button ScaleButton, SpeedButton, DistansButton;
    [SerializeField] private ParticleSystem LevelUpEffect;
    [SerializeField] private ParticleSystem HealthEffect;
   
    public int DistansHook = 0;
    public float SpeedHook = 0;
    public float ScaleHook = 0;
    private int ScaleLevel, SpeedLevel, DistansLevel;
    [SerializeField] private Text ScaleLevelText, SpeedLevelText, DistansLevelText;
    private int levelCount = 1;
    private float MaxCostLevel = 100;
    private float MinCostLevel = 0;
    public int KillCount;
    public int DeadCount;
    private float CKhook = 0;
    private float MaxHeals = 300;
    private float MinHeal = 0;
    public bool Dead = false;
    Vector3 PosSpawn;
    public string Name;
    private float HookAttackActive = 0;
    private int SkillCount = 0;
    private Image BarIcon;
    private Sprite IconLife, IconDead;
    private Text TextDead;
    private float Damage = 100;
    [SerializeField] private Button SpeedMoveButton, HealeSetButton, WampirizmButton,DamageButton,RegenerationButton,BleedingButton;
    [SerializeField] private Text SpeedMoveLevelText, HealeSetLevelText, WampirizmLevelText,DamageLevelText,RegenerationLevelText,BleedingLevelText;
    [SerializeField] private Text GoldText;
    private int Gold = 0;
    private float SpeedMoveLevel = 0;
    private int HealseSetLevel = 0;
    private int WampirizmLevel = 0;
    private int DamageLevel = 0;
    private int RegenerationLevel = 0;
    public int BleedingLevel = 0;
    public float CountSetBleeding = 0;
    private ParticleSystem Blood;
    private int JoystickActive;
    [SerializeField] private AudioSource Audio;
    [SerializeField] private AudioClip[] Clips;
    private int CountKillSound = 0;
    public float GetDamage
    {
        get { return Damage + (DamageLevel*20); }
    }
    void Start()
    {
        MinHeal = MaxHeals;
        Name = PlayerPrefs.GetString("Name", "Name");
        LevelsText.text = levelCount.ToString();
        ButtonActive();
         PosSpawn = transform.position;
        GameController.Instance.SetRedCommand(this.gameObject);
        Bar.fillAmount = MinHeal / MaxHeals;
        ChecGold();
        JoystickActive = PlayerPrefs.GetInt("JoystickActive", 1);
        if (JoystickActive == 1)
        {
            Joystick_2.gameObject.SetActive(true);
        }
        else
        {
            ButtonHook.gameObject.SetActive(true);
        }

    }
    private void ButtonActive()
    {
        ScaleButton.onClick.AddListener(SetScaleHook);
        DistansButton.onClick.AddListener(SetDistansHook);
        SpeedButton.onClick.AddListener(SetSpeedHook);
        SpeedMoveButton.onClick.AddListener(SetSpeedMove);
        HealeSetButton.onClick.AddListener(SetHealeBar);
        WampirizmButton.onClick.AddListener(SetWampirizm);
        DamageButton.onClick.AddListener(SetDamageSkill);
        RegenerationButton.onClick.AddListener(SetRegeneration);
        BleedingButton.onClick.AddListener(SetBleeding);
        ButtonHook.onClick.AddListener(HookClickButton);
    }
    public Image SetBarImage
    {
        set
        {
            BarIcon = value;
            IconLife = GameController.Instance.LifeIcon;
            IconDead = GameController.Instance.DeadIcon;
            BarIcon.sprite = IconLife;
        }
    }
    public void SetKill(int dead)
    {
        if (dead == 1)
        {
            KillCount++;
            Gold += 50;
            GameController.Instance.ActiveAminTextGold(50);
            int SoundActive = PlayerPrefs.GetInt("SoundActive", 1);
            if (SoundActive == 1)
            {
                Audio.clip = Clips[CountKillSound < Clips.Length - 1 ? CountKillSound : Clips.Length - 1];
                Audio.Play();
                CountKillSound++;
            }

            // LevelUp(100); за убийство опыт
        }
        ChecGold();
    }
    public void Hithook(int i) //надо переписать к хуям эту хуету 
    {
        VibrationController.Instance.VibrateWithTypeSelection();
        Gold += 10; //за попадание 10 голд
        GameController.Instance.ActiveAminTextGold(10);
        ChecGold();
    }
    private void ChecGold()
    {
        GoldText.text = Gold.ToString();
        if (Gold >= 100)
        {
            SpeedMoveButton.interactable = true;
            HealeSetButton.interactable = true;
            WampirizmButton.interactable = true;
            DamageButton.interactable = true;
            RegenerationButton.interactable = true;
            BleedingButton.interactable = true;
            
        }
        else
        {
            SpeedMoveButton.interactable = false;
            HealeSetButton.interactable = false;
            WampirizmButton.interactable = false;
            DamageButton.interactable = false;
            RegenerationButton.interactable = false;
            BleedingButton.interactable = false;
        }
    }
    public int SetDamage(float _damage)
    {
        MinHeal -= _damage;
        Bar.fillAmount = MinHeal / MaxHeals;
        if (MinHeal <= 0)
        {
            StopCoroutine(BleedingActive());
            BarIcon.sprite = IconDead;
            DeadCount++;
            Dead = true;
            Anim.SetTrigger("Dead");
            Anim.SetInteger("Stay", 10);
            GetComponent<Rigidbody>().isKinematic = true;
            GetComponent<CapsuleCollider>().enabled = false;
            GameController.Instance.DeletRedCommand(this.gameObject);
            StartCoroutine(Respawn());
            CanvasRotate.enabled = false;
            CountKillSound = 0;
            return 1;
        }
        return 0;
    }
    public void LevelUp(int CostHook)
    {
        ChecWampirizm();
         MinCostLevel += CostHook;
        
        BarLevels.fillAmount = MinCostLevel  / MaxCostLevel;
        if (MaxCostLevel <= MinCostLevel)
        {
            GameController.Instance.OpenSkillContainer(true);
            VibrationController.Instance.VibrateWithTypeHAIDIMPACT();
            GameController.Instance.ActiveAminTextLevelUP();
            levelCount++;
            LevelsText.text = levelCount.ToString();
            LevelUpEffect.Play();
            if (MinCostLevel > MaxCostLevel)
            {
                MinCostLevel = MinCostLevel - MaxCostLevel;
            }
            else
            {
                MinCostLevel = 0;
            }
            MaxCostLevel = 100 * levelCount;
            BarLevels.fillAmount = MinCostLevel / MaxCostLevel;
            SkillCount++;
            DeactiveateButtonSkill();
        }
       

    }
 //skill
    private void SetSpeedHook()
    {
        SpeedHook += 0.02f;
        SpeedLevel++;
        SpeedLevelText.text = SpeedLevel.ToString();
        SkillCount--;
        DeactiveateButtonSkill();
    }
    private void SetDistansHook()
    {
        DistansHook += 10;
        DistansLevel++;
        DistansLevelText.text = DistansLevel.ToString();
        SkillCount--;
        DeactiveateButtonSkill();
      
    }
    private void SetScaleHook()
    {
        ScaleHook += 0.15f;
        ScaleLevel++;
        ScaleLevelText.text = ScaleLevel.ToString();
        SkillCount--;
        DeactiveateButtonSkill();
    }
    private void DeactiveateButtonSkill()
    {
      
        bool active = false;
        if (SkillCount > 0)
        {
            active = true;
        }
        else
        {
            GameController.Instance.OpenSkillContainer(false);
            active = false;
        }
        ScaleButton.interactable = active;
        DistansButton.interactable = active;
        SpeedButton.interactable = active;
        Sun.SetActive(active);
    }

  //shop
    private void SetSpeedMove()
    {
        Speed += 0.5f;
        Gold -= 100;
        SpeedMoveLevel++;
        SpeedMoveLevelText.text = SpeedMoveLevel.ToString();
        ChecGold();
    }
    private void SetHealeBar()
    {
        MaxHeals += 50;
        MinHeal += 50;
        Bar.fillAmount = MinHeal / MaxHeals;
        HealseSetLevel++;
        HealeSetLevelText.text = HealseSetLevel.ToString();
        Gold -= 100;
        ChecGold();
    }
    private void SetWampirizm()
    {
        Gold -= 100;
        WampirizmLevel++;
        WampirizmLevelText.text = WampirizmLevel.ToString();
        ChecGold();
}
    private void ChecWampirizm()
    {
        if (WampirizmLevel > 0)
        {
           
                MinHeal += 10 * WampirizmLevel;
            HealthEffect.Play();
            if (MinHeal > MaxHeals)
            {
                MinHeal = MaxHeals;
            }
            Bar.fillAmount = MinHeal / MaxHeals;
            
        }
    }
    private void SetDamageSkill()
    {
        Gold -= 100;
        DamageLevel++;
        DamageLevelText.text = DamageLevel.ToString();
        ChecGold();
    }
    private void SetRegeneration()
    {
        Gold -= 100;
        RegenerationLevel++;
        RegenerationLevelText.text = RegenerationLevel.ToString();
        ChecGold();
        if(RegenerationLevel == 1)
        {
            StartCoroutine(RegenHp());
        }
    }
    private void SetBleeding()
    {
        Gold -= 100;
        BleedingLevel++;
        BleedingLevelText.text = BleedingLevel.ToString();
        ChecGold();
    }

    public void ChecBleedignActive(bool active , float count)
    {
        CountSetBleeding = 0;
        CountSetBleeding = count*0.7f;
        if (active)
        {
            
            if (Blood == null)
            {
                Blood = Instantiate(GameController.Instance.EffectBlood, transform);
            }
            if (Blood != null)
                Blood.Play();
            StartCoroutine(BleedingActive());
        }
        else
        {
            if (Blood != null)
                Blood.Stop();
            StopCoroutine(BleedingActive());
        }
    }
    private IEnumerator RegenHp()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if(MinHeal<MaxHeals && !Dead)
            {
                MinHeal += RegenerationLevel*2;

                Bar.fillAmount = MinHeal / MaxHeals;
            }
        }
    }
    private IEnumerator Respawn()
    {
        if(TextDead==null)
        TextDead = BarIcon.gameObject.GetComponentInChildren<Text>();
        TextDead.gameObject.SetActive(true);
        for (float i = 6; i > 0; i--)
        {
            TextDead.text = i.ToString();
            yield return new WaitForSeconds(1);
        }
        Dead = false;
        transform.position = PosSpawn;
        MinHeal = MaxHeals;
        GameController.Instance.SetRedCommand(this.gameObject);
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<CapsuleCollider>().enabled = true;
        GetComponent<Rigidbody>().useGravity = true;
        CanvasRotate.enabled = true;
        Bar.fillAmount = MinHeal / MaxHeals;
        BarIcon.sprite = IconLife;
        TextDead.gameObject.SetActive(false);
        StopCoroutine(BleedingActive());
    }
    
    
    void Update()
    {
        transform.position = new Vector3(transform.position.x, 0.2154756f, transform.position.z);
        if (HookAttackActive>=0)
        {
            HookAttackActive -= Time.deltaTime;
        }
        if (GameController.Instance.Stay == GameController.StayGame.Game)
        {
            Sun.transform.Rotate(0, 0, 50 * Time.deltaTime);
            CanvasRotate.transform.LookAt(CanvasRotate.transform.position + Camera.main.transform.rotation * Vector3.back,
                        Camera.main.transform.rotation * Vector3.up);
            HookActive();
            if (!Dead && !GameController.Instance.FinishGameActive && HookAttackActive<0)
            {

                if (Joystick_1.Vertical != 0 && Joystick_1.Horizontal != 0)
                {

                    Vector3 direction = Vector3.forward * 1;
                    transform.Translate(direction * (Speed + 0.1f) * Time.deltaTime);
                    Vector3 DirectRotate = new Vector3(Joystick_1.Horizontal, 0, Joystick_1.Vertical);
                    Quaternion DirRotate = Quaternion.LookRotation((transform.position + DirectRotate) - transform.position);
                    transform.rotation = Quaternion.Slerp(transform.rotation, DirRotate, Time.deltaTime * 15);
                    Anim.SetInteger("Stay", 1);
                }
                else
                {
                    Anim.SetInteger("Stay", 0);
                }
                if (JoystickActive == 1)
                {
                    if (Joystick_2.Vertical != 0 && Joystick_2.Horizontal != 0 && ShootHooks.GetHook == null)
                    {
                        ArroyHook.SetActive(true);
                        Vector3 direction = Vector3.forward * 1;
                        Vector3 DirectRotate = new Vector3(Joystick_2.Horizontal, 0, Joystick_2.Vertical);
                        Quaternion DirRotate = Quaternion.LookRotation((ArroyHook.transform.position + DirectRotate) - ArroyHook.transform.position);
                        ArroyHook.transform.rotation = DirRotate;
                    }
                    else
                    {
                        ArroyHook.SetActive(false);
                    }
                }
            }
        }
     
    }
    private void HookActive()
    {
        CKhook -= Time.deltaTime;
        KDHOOkTExt.text = CKhook.ToString("#");
        KdhookText_2.text = CKhook.ToString("#");
        if (CKhook <= 0 )
        {
            if (JoystickActive == 1)
            {
                JoystickIcon.SetActive(true);
                KdhookText_2.enabled = false;
                Joystick_2.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                Joystick_2.GetComponent<FixedJoystick>().enabled = true;
            }
            else
            {
                ButtonHook.interactable = true;
                KDHOOkTExt.enabled = false;
                BakkGroundHookTimer.enabled = false;
            }
               
        }
        else
        {
            if (JoystickActive == 1)
            {
                JoystickIcon.SetActive(false);
                KdhookText_2.enabled = true;
                Joystick_2.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                Joystick_2.GetComponent<FixedJoystick>().enabled = false;
            }
            else
            {
                ButtonHook.interactable = false;
                KDHOOkTExt.enabled = true;
                BakkGroundHookTimer.enabled = true;
            }
        }
    }
    private IEnumerator BleedingActive()
    {
        while (MinHeal > 0)
        {
            yield return new WaitForSeconds(0.3f);
            if (MinHeal > 0 && !Dead)
            {
                MinHeal -= CountSetBleeding;
                Bar.fillAmount = MinHeal / MaxHeals;
                SetDamage(0);
            }
            else
            {
                Dead = true;
                StopCoroutine(BleedingActive());
            }
        }
        
    }
    public void HookClickButton()
    {
        if (GameController.Instance.Stay == GameController.StayGame.Game)
        {
            if (!Dead)
            {
                if (ShootHooks.GetHook == null)
                {
                    if (JoystickActive == 1)
                    {
                        transform.rotation = ArroyHook.transform.rotation;
                        ArroyHook.SetActive(false);
                        CKhook = 4;
                        Anim.SetTrigger("Hook");
                        ShootHooks.Hooook();
                        HookAttackActive = 0.2f;
                    }
                    else
                    {
                        CKhook = 4;
                        Anim.SetTrigger("Hook");
                        ShootHooks.Hooook();
                        HookAttackActive = 0.2f;
                    }
                
                }
            }
        }
    }
}
